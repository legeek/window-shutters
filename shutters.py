"""
Four GPIOs are going to be used here.
(BCM Mode)
    Port 18 is connected to UP
    Port 23 is connected to STOP
    Port 24 is connected to DOWN
    Port 25 is connected to NEXT
"""
import argparse

parser = argparse.ArgumentParser(
        description="This script controls window shutters of my house",
        epilog="Repository at https://bitbucket.org/legeek/window-shutters")
parser.add_argument('-a', '--action', help='Choose what to do.', type=str, choices=['up', 'down'], required=True)
parser.add_argument('-w', '--what', help='Against what object to run the action.', type=str, choices=['all', 'bay', 'lateral', 'kitchen', 'topfloor'], default='all')
args=parser.parse_args()

import time

try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  You need superuser privileges.")

def togglegpio(gpionr):
    """ Changes the status of a GPIO given as argument to High then half a
    second later changes back the status to Low """
    GPIO.output(gpionr, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(gpionr, GPIO.LOW)
    return;

def cycle(what):
    print("Cycling")
    togglegpio(gstop)
    cyclecount=0
    while cyclecount!=rotation[what]:
        togglegpio(gnext)
        cyclecount+=1
        time.sleep(1)
    return;

def cycleback(what):
    print("Cycling back")
    togglegpio(gstop)
    cyclecount=6-rotation[what]
    while cyclecount!=0:
        togglegpio(gnext)
        cyclecount-=1
        time.sleep(1)
    return;

def down(what):
    togglegpio(gstop)
    cycle(what)
    togglegpio(gdown)
    cycleback(what)

def up(what):
    togglegpio(gstop)
    cycle(what)
    togglegpio(gup)
    cycleback(what)

def down_several(ensemble):
    for item in ensemble:
        down(item)

def up_several(ensemble):
    for item in ensemble:
        up(item)

""" initialisation of the GPIO ports """
gup=18
gstop=23
gdown=24
gnext=25
chan_list = [gup, gstop, gdown, gnext]
rotation={'all':0, 'bay':1, 'lateral': 2, 'kitchen':3, 'topfloor':5}

""" board initialisation """
print("Cleaning up BCM")
GPIO.cleanup(chan_list)
print("Setting numbering mode to BCM")
GPIO.setmode(GPIO.BCM)
print("Setting channels to GPIO.OUT Mode")
GPIO.setup(chan_list, GPIO.OUT)

""" Always use an ensemble to command shutters """
"""up_several({'all'}) """

GPIO.cleanup(chan_list)

