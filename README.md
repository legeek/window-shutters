## Motivation

I have an electrical window shutters installation at home that sadly uses a
proprietary protocol. I then decided to use a multi channel remote command and
wire the buttons to the GPIO ports of a Raspberry PI.

## Reference

[Raspberry Pi pinout](https://pinout.xyz)

[Window shutter makers](https://www.becker-antriebe.de/)

## Licence
GPL. Feel free to modify/enhance the code. I'm no developper so please be
indulgeant.
